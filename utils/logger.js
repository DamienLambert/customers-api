const winston = require('winston')

let logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.colorize(),
    winston.format.align(),
    winston.format.printf(info => {
      return `${info.timestamp} ${info.level}: ${info.message}`
    })
  ),
  transports: [
    new winston.transports.Console({ colorize: true }) //, new winston.transports.File({ filename: './logs/combined.log' })
  ]
})

module.exports = logger