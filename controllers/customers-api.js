// External imports
const express = require('express')
const router = express.Router()

// Local imports
const customersService = require('../services/customers-service')

/**
 * Gets all the customers
 * Returns 200 if found, otherwise it sends 204
 */
router.get('/', (req, res) => {
  // Retrieves all the customers
  customersService.getCustomers()
    .then((content) => {
      console.log(content)
      res.send(content)
    }, (error) => {
      res.status(204).send(error)
    })
})

/**
 * Gets one customer
 * Returns 200 if found, 404 if not found and 500 when a server error occurs
 */
router.get('/:id', (req, res) => {
  // Retrieves the customer's id
  const id = req.params.id

  // Retrieves the customer
  customersService.getCustomer(id)
    .then((content) => {
      if (content) {
        res.send(content)
      } else {
        res.status(404).send()
      }
    }, (error) => {
      res.status(500).send(error)
    })
})

/**
 * Adds a customer
 * It returns 200 along with the added customer
 */
router.post('/', (req, res) => {
  // Retrieves the body that contains the "firstName" and the "lastName"
  const aCustomer = req.body

  // Adds the customer
  customersService
    .addCustomer(aCustomer.firstName, aCustomer.lastName)
    .then((response) => res.send(response))
    .catch((e) => res.send(500, e))
})

/**
 * Edits one customer
 * Returns 500 when a server error occure, otherwise it sends a 200 along with the edited customer
 */
router.put('/:id', (req, res) => {
  // Retrieves the customer's id
  const id = req.params.id

  // Retrieves the edited customer
  const aCustomer = req.body

  // Edits the customer
  customersService.editCustomer(id, aCustomer.firstName, aCustomer.lastName, (err, model) => {
    if (err) {
      res.send(500, err)
    } else {
      res.send(200, model)
    }
  })
})

/**
 * Deletes one customer
 * Returns 500 when a server error occurs, otherwhise it always sends 204
 */
router.delete('/:id', (req, res) => {
  const id = req.params.id
  customersService.removeCustomer(id, (err) => {
    if (err) {
      console.log('Problems while deleting the customer')
      res.send(500, err)
    } else {
      console.log('Customer successfully deleted')
      res.send(204)
    }
  })
})

module.exports = router