const express = require('express')
const router = express.Router()

router.use(require('./request-logger'))

module.exports = router