
// External imports
const mongoose = require('mongoose')
const express = require('express')
const bodyParser = require('body-parser')
//const helmet = require('helmet')

// Internal imports
const config = require('./config')
const logger = require('./utils/logger')

logger.info('Starting customer-api ... ')

// Mongodb connection
mongoose.connect(config.mongodbUri, { useNewUrlParser: true })

// Express init
const port = process.env.PORT || 8080
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

// Helmet init
//app.use(helmet())

// Middlewares
app.use('/businessapi/v1', require('./middlewares'))

// Controllers
app.use('/businessapi/v1', require('./controllers'))

app.listen(port, () => logger.info(`Listening on port ${port}!`))

logger.info('customer-api ended ... ')