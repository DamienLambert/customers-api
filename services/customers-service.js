const Customer = require('../models/customer-model')
const logger = require('../utils/logger')

/**
 * A function that adds a customer
 * @param {string} firstName
 * @param {string} lastName
 * @returns a promise for the "writeFile" operation
 */
const addCustomer = async (firstName = '', lastName = '') => {
  // Init the model that will hold the data value
  const aCustomer = new Customer()
  aCustomer.firstName = firstName
  aCustomer.lastName = lastName

  // Get promise result
  const result = await aCustomer.save()

  // Return the resolved result
  return result
}

/**
 * A function that returns the customers
 * The first promise reads the directory. If it's resolved, it returns an array of promises for reading every file
 * @returns an array of promises for the "readFile" operation
 */
const getCustomer = async (id) => {
  // Get promise result
  const result = await Customer.findById(id)

  // Return the resolved promise
  return result
}

/**
 * A function that returns the customers
 * The first promise reads the directory. If it's resolved, it returns an array of promises for reading every file
 * @returns an array of promises for the "readFile" operation
 */
const getCustomers = async () => {
  // Get promise result
  const result = await Customer.find()

  // Return the resolved promise
  return result
}

/**
 * Edits a customer.
 * In fact, it removes it and then recreates it from scratch
 * @param {*} id customer's id to edit
 * @param {*} firstName  customer's first name
 * @param {*} lastName customer's last name
 * @param {*} done callback of the caller. The first argument is for errors :)
 */
const editCustomer = (id, firstName, lastName, done) => {
  const fieldsToUpdate = {
    firstName, lastName
  }

  Customer.findByIdAndUpdate(id, fieldsToUpdate, { new: true }, (err, model) => {a
    if (err) {
      logger.error(`Error while updating ${id} : ${err}`)
      done(`Problem while updating ${id}`)
    } else done(undefined, model)
  })
}

/**
 * Deletes a customer.
 * This time, we used a callack to demo it
 * @param {string} id
 * @param {*} done
 */
const removeCustomer = (id, done) => {
  Customer.findByIdAndDelete(id, (err) => {
    if (err) done(`Problem while deleting ${id}`)
    else done(undefined)
  })
}

/**
 * Exporting the public methods
 */
module.exports = {
  addCustomer,
  getCustomer,
  getCustomers,
  editCustomer,
  removeCustomer
}